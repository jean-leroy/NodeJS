# NodeJS Project

## Installation

1. git clone the final version of the repository
2. npm install
3. npm run populate to generate data
4. npm start

 ## Testing 

 Unit tests are implemented for metrics operations and user operations. Run them with npm run test

 ## App tutorial
 
 1. Start by creating an account or logging in to "arnaud" or "jean" (password="pass")
 2. On the metrics page, you can add, update or delete metrics and of course visualize them in a graph
 3. On the accout page, you can modify your email adress or delete your account
 4. Log in as an admin (username = "admin", password ="admin") to check the admin page. You can delete any user here.

Arnaud Emprin and Jean Leroy
SI International Group 1
ECE Paris
